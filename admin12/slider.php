 <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="Dashboard.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="dashboard.php"><i class="fa fa-circle-o"></i> Dashboard</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Driver</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">2</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="bike_driver.php"><i class="fa fa-circle-o"></i>Bike</a></li>
            <li><a href="cab_driver.php"><i class="fa fa-circle-o"></i>Cab</a></li>

          </ul>
        </li>

<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Driver Account</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">2</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="bike_driver_man.php"><i class="fa fa-circle-o"></i>Bike</a></li>
            <li><a href="cab_driver_man.php"><i class="fa fa-circle-o"></i>Cab</a></li>

          </ul>
        </li>

        <li>
          <a href="#">
            <i class="fa fa-th"></i> <span>Users</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green">New</small>
            </span>
          </a>

        </li>
        <li class="treeview">
          <a href="">
            <i class="fa fa-pie-chart"></i>
            <span>Transaction </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="tran.php"><i class="fa fa-circle-o"></i>ALL</a></li>
            <li><a href="5.php"><i class="fa fa-circle-o"></i>Cancel Ride</a></li>
            <li><a href="7.php"><i class="fa fa-circle-o"></i>Complete Ride</a></li>
            
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>User Manage</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Active User</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Un Active User</a></li>
            
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Driver Mange</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
            <li><a href="http://localhost/leryde/index.php/c_utama/join" target="_blank"><i class="fa fa-circle-o"></i>Add New Driver</a></li>
          </ul>
        </li>
        
    </section>
    <!-- /.sidebar -->