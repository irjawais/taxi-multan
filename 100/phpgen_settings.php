<?php

//  define('SHOW_VARIABLES', 1);
//  define('DEBUG_LEVEL', 1);

//  error_reporting(E_ALL ^ E_NOTICE);
//  ini_set('display_errors', 'On');

set_include_path('.' . PATH_SEPARATOR . get_include_path());


include_once dirname(__FILE__) . '/' . 'components/utils/system_utils.php';
include_once dirname(__FILE__) . '/' . 'components/mail/mailer.php';
include_once dirname(__FILE__) . '/' . 'components/mail/phpmailer_based_mailer.php';
require_once dirname(__FILE__) . '/' . 'database_engine/mysql_engine.php';

//  SystemUtils::DisableMagicQuotesRuntime();

SystemUtils::SetTimeZoneIfNeed('Asia/Yekaterinburg');

function GetGlobalConnectionOptions()
{
    return
        array(
          'server' => 'localhost',
          'port' => '3306',
          'username' => 'leryde_go_taxi',
          'password' => 'leryde_go_taxi',
          'database' => 'leryde_go_taxi',
          'client_encoding' => 'utf8'
        );
}

function HasAdminPage()
{
    return false;
}

function HasHomePage()
{
    return true;
}

function GetHomeURL()
{
    return 'dashboard.php';
}

function GetHomePageBanner()
{
    return '';
}

function GetPageGroups()
{
    $result = array();
    $result[] = array('caption' => 'Default', 'description' => '');
    return $result;
}

function GetPageInfos()
{
    $result = array();
    $result[] = array('caption' => 'Ac Type', 'short_caption' => 'Ac Type', 'filename' => 'ac_type.php', 'name' => 'ac_type', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Additional Config', 'short_caption' => 'Additional Config', 'filename' => 'additional_config.php', 'name' => 'additional_config', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Admin', 'short_caption' => 'Admin', 'filename' => 'admin.php', 'name' => 'admin', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Company\'s address
    ', 'short_caption' => 'Company\'s address
    ', 'filename' => 'alamat_perusahaan.php', 'name' => 'alamat_perusahaan', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'App Versions', 'short_caption' => 'App Versions', 'filename' => 'app_versions.php', 'name' => 'app_versions', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Blog Content', 'short_caption' => 'Blog Content', 'filename' => 'blog_content.php', 'name' => 'blog_content', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Branch company
    ', 'short_caption' => 'Branch company
    ', 'filename' => 'Branch company.php', 'name' => 'cabang_perusahaan', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Config Driver', 'short_caption' => 'Config Driver', 'filename' => 'config_driver.php', 'name' => 'config_driver', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Data Temp', 'short_caption' => 'Data Temp', 'filename' => 'data_temp.php', 'name' => 'data_temp', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Driver', 'short_caption' => 'Driver', 'filename' => 'driver.php', 'name' => 'driver', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Driver Job', 'short_caption' => 'Driver Job', 'filename' => 'driver_job.php', 'name' => 'driver_job', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Promotion Feature
    ', 'short_caption' => 'Promotion Feature
    ', 'filename' => 'fitur_promosi.php', 'name' => 'fitur_promosi', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Help General', 'short_caption' => 'Help General', 'filename' => 'help_general.php', 'name' => 'help_general', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Customer Help
    ', 'short_caption' => 'Customer Help
    ', 'filename' => 'help_pelanggan.php', 'name' => 'help_pelanggan', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Transaction History
    ', 'short_caption' => 'Transaction History
    ', 'filename' => 'Transaction History.php', 'name' => 'history_transaksi', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Transportation type
    ', 'short_caption' => 'Transportation type
    ', 'filename' => 'jenis_kendaraan.php', 'name' => 'jenis_kendaraan', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Vehicle', 'short_caption' => 'Vehicle', 'filename' => 'Vehicle.php', 'name' => 'kendaraan', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Customer Location
    ', 'short_caption' => 'Customer Location
    ', 'filename' => 'lokasi_pelanggan.php', 'name' => 'lokasi_pelanggan', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Customer', 'short_caption' => 'Customer', 'filename' => 'Customer.php', 'name' => 'pelanggan', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Company Profile
    ', 'short_caption' => 'Company Profile
    ', 'filename' => 'profil_perusahaan.php', 'name' => 'profil_perusahaan', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Promotion', 'short_caption' => 'Promotion', 'filename' => 'promosi.php', 'name' => 'promosi', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Cost Proportion
    ', 'short_caption' => 'Cost Proportion
    ', 'filename' => 'proporsi_biaya.php', 'name' => 'proporsi_biaya', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Rating Driver', 'short_caption' => 'Rating Driver', 'filename' => 'rating_driver.php', 'name' => 'rating_driver', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Customer Ratings
    ', 'short_caption' => 'Customer Ratings
    ', 'filename' => 'rating_pelanggan.php', 'name' => 'rating_pelanggan', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Balance', 'short_caption' => 'Balance', 'filename' => 'Balance.php', 'name' => 'saldo', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Status Driver', 'short_caption' => 'Status Driver', 'filename' => 'status_driver.php', 'name' => 'status_driver', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Status Topup', 'short_caption' => 'Status Topup', 'filename' => 'status_topup.php', 'name' => 'status_topup', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Status Transaction', 'short_caption' => 'Status Transaction', 'filename' => 'status_transaksi.php', 'name' => 'status_transaksi', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Status User', 'short_caption' => 'Status User', 'filename' => 'status_user.php', 'name' => 'status_user', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Status Withdraw', 'short_caption' => 'Status Withdraw', 'filename' => 'status_withdraw.php', 'name' => 'status_withdraw', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Transaction Type
    ', 'short_caption' => 'Transaction Type
    ', 'filename' => 'tipe_transaksi.php', 'name' => 'tipe_transaksi', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Topup', 'short_caption' => 'Topup', 'filename' => 'topup.php', 'name' => 'topup', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Transaction', 'short_caption' => 'Transaction', 'filename' => 'transaksi.php', 'name' => 'transaksi', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Driver Transaction
    ', 'short_caption' => 'Driver Transaction
    ', 'filename' => 'transaksi_driver.php', 'name' => 'transaksi_driver', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Customer Transaction
    ', 'short_caption' => 'Customer Transaction
    ', 'filename' => 'transaksi_pelanggan.php', 'name' => 'transaksi_pelanggan', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Voucher', 'short_caption' => 'Voucher', 'filename' => 'voucher.php', 'name' => 'voucher', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Massaging Service Time
    ', 'short_caption' => 'Massaging Service Time
    ', 'filename' => 'waktu_pelayanan_pemijat.php', 'name' => 'waktu_pelayanan_pemijat', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Withdraw', 'short_caption' => 'Withdraw', 'filename' => 'withdraw.php', 'name' => 'withdraw', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Ac Type', 'short_caption' => 'Ac Type', 'filename' => 'ac_type01.php', 'name' => 'ac_type01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Additional Config', 'short_caption' => 'Additional Config', 'filename' => 'additional_config01.php', 'name' => 'additional_config01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Admin', 'short_caption' => 'Admin', 'filename' => 'admin01.php', 'name' => 'admin01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Alamat Perusahaan', 'short_caption' => 'Alamat Perusahaan', 'filename' => 'alamat_perusahaan01.php', 'name' => 'alamat_perusahaan01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'App Versions', 'short_caption' => 'App Versions', 'filename' => 'app_versions01.php', 'name' => 'app_versions01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Asuransi', 'short_caption' => 'Asuransi', 'filename' => 'asuransi01.php', 'name' => 'asuransi01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Barang Toko', 'short_caption' => 'Barang Toko', 'filename' => 'barang_toko01.php', 'name' => 'barang_toko01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Belanja Mfood', 'short_caption' => 'Belanja Mfood', 'filename' => 'belanja_mfood01.php', 'name' => 'belanja_mfood01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Belanja Mmart', 'short_caption' => 'Belanja Mmart', 'filename' => 'belanja_mmart01.php', 'name' => 'belanja_mmart01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Belanja Mstore', 'short_caption' => 'Belanja Mstore', 'filename' => 'belanja_mstore01.php', 'name' => 'belanja_mstore01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Berkas Lamaran Kerja', 'short_caption' => 'Berkas Lamaran Kerja', 'filename' => 'berkas_lamaran_kerja01.php', 'name' => 'berkas_lamaran_kerja01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Biaya Promo', 'short_caption' => 'Biaya Promo', 'filename' => 'biaya_promo01.php', 'name' => 'biaya_promo01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Blog Content', 'short_caption' => 'Blog Content', 'filename' => 'blog_content01.php', 'name' => 'blog_content01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Cabang Perusahaan', 'short_caption' => 'Cabang Perusahaan', 'filename' => 'cabang_perusahaan.php', 'name' => 'cabang_perusahaan01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Config Driver', 'short_caption' => 'Config Driver', 'filename' => 'config_driver01.php', 'name' => 'config_driver01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Data Temp', 'short_caption' => 'Data Temp', 'filename' => 'data_temp01.php', 'name' => 'data_temp01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Driver', 'short_caption' => 'Driver', 'filename' => 'driver01.php', 'name' => 'driver01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Driver Job', 'short_caption' => 'Driver Job', 'filename' => 'driver_job01.php', 'name' => 'driver_job01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Fitur Mangjek', 'short_caption' => 'Fitur Mangjek', 'filename' => 'fitur_mangjek01.php', 'name' => 'fitur_mangjek01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Fitur Mservice', 'short_caption' => 'Fitur Mservice', 'filename' => 'fitur_mservice01.php', 'name' => 'fitur_mservice01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Fitur Promosi', 'short_caption' => 'Fitur Promosi', 'filename' => 'fitur_promosi01.php', 'name' => 'fitur_promosi01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Gender Pemijat', 'short_caption' => 'Gender Pemijat', 'filename' => 'gender_pemijat01.php', 'name' => 'gender_pemijat01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Help General', 'short_caption' => 'Help General', 'filename' => 'help_general01.php', 'name' => 'help_general01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Help Pelanggan', 'short_caption' => 'Help Pelanggan', 'filename' => 'help_pelanggan01.php', 'name' => 'help_pelanggan01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'History Transaksi', 'short_caption' => 'History Transaksi', 'filename' => 'history_transaksi.php', 'name' => 'history_transaksi01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Jenis Kendaraan', 'short_caption' => 'Jenis Kendaraan', 'filename' => 'jenis_kendaraan01.php', 'name' => 'jenis_kendaraan01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Kategori Barang', 'short_caption' => 'Kategori Barang', 'filename' => 'kategori_barang01.php', 'name' => 'kategori_barang01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Kategori Resto', 'short_caption' => 'Kategori Resto', 'filename' => 'kategori_resto01.php', 'name' => 'kategori_resto01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Kategori Toko', 'short_caption' => 'Kategori Toko', 'filename' => 'kategori_toko01.php', 'name' => 'kategori_toko01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Kendaraan', 'short_caption' => 'Kendaraan', 'filename' => 'kendaraan.php', 'name' => 'kendaraan01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Layanan Pijat', 'short_caption' => 'Layanan Pijat', 'filename' => 'layanan_pijat01.php', 'name' => 'layanan_pijat01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Lokasi Pelanggan', 'short_caption' => 'Lokasi Pelanggan', 'filename' => 'lokasi_pelanggan01.php', 'name' => 'lokasi_pelanggan01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Makanan', 'short_caption' => 'Makanan', 'filename' => 'makanan01.php', 'name' => 'makanan01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Menu Makanan', 'short_caption' => 'Menu Makanan', 'filename' => 'menu_makanan01.php', 'name' => 'menu_makanan01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Mitra Mmart Mfood', 'short_caption' => 'Mitra Mmart Mfood', 'filename' => 'mitra_mmart_mfood01.php', 'name' => 'mitra_mmart_mfood01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Mservice Jenis', 'short_caption' => 'Mservice Jenis', 'filename' => 'mservice_jenis01.php', 'name' => 'mservice_jenis01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Pelanggan', 'short_caption' => 'Pelanggan', 'filename' => 'pelanggan.php', 'name' => 'pelanggan01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Pemijat In Keahlian', 'short_caption' => 'Pemijat In Keahlian', 'filename' => 'pemijat_in_keahlian01.php', 'name' => 'pemijat_in_keahlian01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Pendaftaran Acservice', 'short_caption' => 'Pendaftaran Acservice', 'filename' => 'pendaftaran_acservice01.php', 'name' => 'pendaftaran_acservice01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Pendaftaran Mmassage', 'short_caption' => 'Pendaftaran Mmassage', 'filename' => 'pendaftaran_mmassage01.php', 'name' => 'pendaftaran_mmassage01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Peralatan Service', 'short_caption' => 'Peralatan Service', 'filename' => 'peralatan_service01.php', 'name' => 'peralatan_service01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Phpgen Users', 'short_caption' => 'Phpgen Users', 'filename' => 'phpgen_users.php', 'name' => 'phpgen_users', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Profil Perusahaan', 'short_caption' => 'Profil Perusahaan', 'filename' => 'profil_perusahaan01.php', 'name' => 'profil_perusahaan01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Promosi', 'short_caption' => 'Promosi', 'filename' => 'promosi01.php', 'name' => 'promosi01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Promosi Mfood', 'short_caption' => 'Promosi Mfood', 'filename' => 'promosi_mfood.php', 'name' => 'promosi_mfood01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Proporsi Biaya', 'short_caption' => 'Proporsi Biaya', 'filename' => 'proporsi_biaya01.php', 'name' => 'proporsi_biaya01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Rating Driver', 'short_caption' => 'Rating Driver', 'filename' => 'rating_driver01.php', 'name' => 'rating_driver01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Rating Pelanggan', 'short_caption' => 'Rating Pelanggan', 'filename' => 'rating_pelanggan01.php', 'name' => 'rating_pelanggan01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Redeemed Voucher', 'short_caption' => 'Redeemed Voucher', 'filename' => 'redeemed_voucher01.php', 'name' => 'redeemed_voucher01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Restoran', 'short_caption' => 'Restoran', 'filename' => 'restoran.php', 'name' => 'restoran01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Saldo', 'short_caption' => 'Saldo', 'filename' => 'saldo.php', 'name' => 'saldo01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Status Driver', 'short_caption' => 'Status Driver', 'filename' => 'status_driver01.php', 'name' => 'status_driver01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Status Topup', 'short_caption' => 'Status Topup', 'filename' => 'status_topup01.php', 'name' => 'status_topup01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Status Transaksi', 'short_caption' => 'Status Transaksi', 'filename' => 'status_transaksi01.php', 'name' => 'status_transaksi01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Status User', 'short_caption' => 'Status User', 'filename' => 'status_user01.php', 'name' => 'status_user01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Status Withdraw', 'short_caption' => 'Status Withdraw', 'filename' => 'status_withdraw01.php', 'name' => 'status_withdraw01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Teknisi', 'short_caption' => 'Teknisi', 'filename' => 'teknisi.php', 'name' => 'teknisi01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Teknisi In Jenis Service', 'short_caption' => 'Teknisi In Jenis Service', 'filename' => 'teknisi_in_jenis_service01.php', 'name' => 'teknisi_in_jenis_service01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Tipe Transaksi', 'short_caption' => 'Tipe Transaksi', 'filename' => 'tipe_transaksi01.php', 'name' => 'tipe_transaksi01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Tipe Voucher', 'short_caption' => 'Tipe Voucher', 'filename' => 'tipe_voucher01.php', 'name' => 'tipe_voucher01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Toko', 'short_caption' => 'Toko', 'filename' => 'toko01.php', 'name' => 'toko01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Topup', 'short_caption' => 'Topup', 'filename' => 'topup01.php', 'name' => 'topup01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Transaksi', 'short_caption' => 'Transaksi', 'filename' => 'transaksi01.php', 'name' => 'transaksi01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Transaksi Detail Mbox', 'short_caption' => 'Transaksi Detail Mbox', 'filename' => 'transaksi_detail_mbox01.php', 'name' => 'transaksi_detail_mbox01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Transaksi Detail Mbox Destinasi', 'short_caption' => 'Transaksi Detail Mbox Destinasi', 'filename' => 'transaksi_detail_mbox_destinasi01.php', 'name' => 'transaksi_detail_mbox_destinasi01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Transaksi Detail Mfood', 'short_caption' => 'Transaksi Detail Mfood', 'filename' => 'transaksi_detail_mfood01.php', 'name' => 'transaksi_detail_mfood01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Transaksi Detail Mmart', 'short_caption' => 'Transaksi Detail Mmart', 'filename' => 'transaksi_detail_mmart01.php', 'name' => 'transaksi_detail_mmart01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Transaksi Detail Mmassage', 'short_caption' => 'Transaksi Detail Mmassage', 'filename' => 'transaksi_detail_mmassage01.php', 'name' => 'transaksi_detail_mmassage01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Transaksi Detail Msend', 'short_caption' => 'Transaksi Detail Msend', 'filename' => 'transaksi_detail_msend01.php', 'name' => 'transaksi_detail_msend01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Transaksi Detail Mservice Ac', 'short_caption' => 'Transaksi Detail Mservice Ac', 'filename' => 'transaksi_detail_mservice_ac01.php', 'name' => 'transaksi_detail_mservice_ac01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Transaksi Detail Mstore', 'short_caption' => 'Transaksi Detail Mstore', 'filename' => 'transaksi_detail_mstore01.php', 'name' => 'transaksi_detail_mstore01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Transaksi Driver', 'short_caption' => 'Transaksi Driver', 'filename' => 'transaksi_driver01.php', 'name' => 'transaksi_driver01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Transaksi Pelanggan', 'short_caption' => 'Transaksi Pelanggan', 'filename' => 'transaksi_pelanggan01.php', 'name' => 'transaksi_pelanggan01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Uang Belanja', 'short_caption' => 'Uang Belanja', 'filename' => 'uang_belanja01.php', 'name' => 'uang_belanja01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Voucher', 'short_caption' => 'Voucher', 'filename' => 'voucher01.php', 'name' => 'voucher01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Waktu Pelayanan Pemijat', 'short_caption' => 'Waktu Pelayanan Pemijat', 'filename' => 'waktu_pelayanan_pemijat01.php', 'name' => 'waktu_pelayanan_pemijat01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    $result[] = array('caption' => 'Withdraw', 'short_caption' => 'Withdraw', 'filename' => 'withdraw01.php', 'name' => 'withdraw01', 'group_name' => 'Default', 'add_separator' => false, 'description' => '');
    return $result;
}

function GetPagesHeader()
{
    return
        'Leryde Admin';
}

function GetPagesFooter()
{
    return
        'Software by Paksitehub';
}

function ApplyCommonPageSettings(Page $page, Grid $grid)
{
    $page->SetShowUserAuthBar(true);
    $page->setShowNavigation(true);
    $page->OnCustomHTMLHeader->AddListener('Global_CustomHTMLHeaderHandler');
    $page->OnGetCustomTemplate->AddListener('Global_GetCustomTemplateHandler');
    $page->OnGetCustomExportOptions->AddListener('Global_OnGetCustomExportOptions');
    $page->getDataset()->OnGetFieldValue->AddListener('Global_OnGetFieldValue');
    $page->getDataset()->OnGetFieldValue->AddListener('OnGetFieldValue', $page);
    $grid->BeforeUpdateRecord->AddListener('Global_BeforeUpdateHandler');
    $grid->BeforeDeleteRecord->AddListener('Global_BeforeDeleteHandler');
    $grid->BeforeInsertRecord->AddListener('Global_BeforeInsertHandler');
    $grid->AfterUpdateRecord->AddListener('Global_AfterUpdateHandler');
    $grid->AfterDeleteRecord->AddListener('Global_AfterDeleteHandler');
    $grid->AfterInsertRecord->AddListener('Global_AfterInsertHandler');
}

function GetAnsiEncoding() { return 'windows-1252'; }

function Global_OnGetCustomPagePermissionsHandler(Page $page, PermissionSet &$permissions, &$handled)
{

}

function Global_CustomHTMLHeaderHandler($page, &$customHtmlHeaderText)
{

}

function Global_GetCustomTemplateHandler($type, $part, $mode, &$result, &$params, CommonPage $page = null)
{

}

function Global_OnGetCustomExportOptions($page, $exportType, $rowData, &$options)
{

}

function Global_OnGetFieldValue($fieldName, &$value, $tableName)
{

}

function Global_GetCustomPageList(CommonPage $page, PageList $pageList)
{

}

function Global_BeforeInsertHandler($page, &$rowData, $tableName, &$cancel, &$message, &$messageDisplayTime)
{

}

function Global_BeforeUpdateHandler($page, $oldRowData, &$rowData, $tableName, &$cancel, &$message, &$messageDisplayTime)
{

}

function Global_BeforeDeleteHandler($page, &$rowData, $tableName, &$cancel, &$message, &$messageDisplayTime)
{

}

function Global_AfterInsertHandler($page, $rowData, $tableName, &$success, &$message, &$messageDisplayTime)
{

}

function Global_AfterUpdateHandler($page, $oldRowData, $rowData, $tableName, &$success, &$message, &$messageDisplayTime)
{

}

function Global_AfterDeleteHandler($page, $rowData, $tableName, &$success, &$message, &$messageDisplayTime)
{

}

function GetDefaultDateFormat()
{
    return 'Y-m-d';
}

function GetFirstDayOfWeek()
{
    return 0;
}

function GetPageListType()
{
    return PageList::TYPE_SIDEBAR;
}

function GetNullLabel()
{
    return null;
}

function UseMinifiedJS()
{
    return true;
}

function GetOfflineMode()
{
    return false;
}

function GetInactivityTimeout()
{
    return 0;
}

function GetMailer()
{
    $smtpOptions = new SMTPOptions('mail.leryde.com', 587, true, 'admin@leryde.com', 'Lenovo@123', '');
    $mailerOptions = new MailerOptions(MailerType::SMTP, 'admin@leryde.com', 'Administrator', $smtpOptions);
    
    return PHPMailerBasedMailer::getInstance($mailerOptions);
}

function sendMailMessage($recipients, $messageSubject, $messageBody, $attachments = '', $cc = '', $bcc = '')
{
    GetMailer()->send($recipients, $messageSubject, $messageBody, $attachments, $cc, $bcc);
}

function createConnection()
{
    $connectionOptions = GetGlobalConnectionOptions();
    $connectionOptions['client_encoding'] = 'utf8';

    $connectionFactory = MySqlIConnectionFactory::getInstance();
    return $connectionFactory->CreateConnection($connectionOptions);
}
